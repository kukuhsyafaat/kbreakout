# Translation of kbreakout to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2008, 2009, 2010, 2016, 2017, 2022.
# Eirik U. Birkeland <eirbir@gmail.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: kbreakout\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-20 00:44+0000\n"
"PO-Revision-Date: 2022-07-26 20:56+0200\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.04.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Karl Ove Hufthammer"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "karl@huftis.org"

#. i18n: ectx: property (windowTitle), widget (QWidget, GeneralSettings)
#: generalsettings.ui:13
#, kde-format
msgid "General Settings"
msgstr "Generelle innstillingar"

#. i18n: ectx: property (text), widget (QLabel, label)
#: generalsettings.ui:28
#, kde-format
msgid ""
"Enabling the following option will make the game steal the mouse cursor, "
"pause the game to get the cursor back."
msgstr ""
"Viss du slår på dette valet, vil spelet stela musepeikaren. Set spelet i "
"pause for å få han tilbake."

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_FireOnClick)
#. i18n: ectx: label, entry (FireOnClick), group (General)
#: generalsettings.ui:38 kbreakout.kcfg:9
#, kde-format
msgid "Fire on mouse click"
msgstr "Skyt ved museklikk"

#: main.cpp:38
#, kde-format
msgid "KBreakOut"
msgstr "KBreakOut"

#: main.cpp:39
#, kde-format
msgid "A Breakout like game by KDE"
msgstr "Breakout-liknande spel frå KDE"

#: main.cpp:41
#, kde-format
msgid "(c) 2007-2008 Fela Winkelmolen"
msgstr "© 2007–2008 Fela Winkelmolen"

#: main.cpp:42
#, kde-format
msgid "Fela Winkelmolen"
msgstr "Fela Winkelmolen"

#: main.cpp:43
#, kde-format
msgid "original author and maintainer"
msgstr "Opphavsperson og vedlikehaldar"

#: main.cpp:45
#, kde-format
msgid "Eugene Trounev"
msgstr "Eugene Trounev"

#: main.cpp:46 main.cpp:49
#, kde-format
msgid "artwork"
msgstr "Bilete"

#: main.cpp:48
#, kde-format
msgid "Sean Wilson"
msgstr "Sean Wilson"

#: main.cpp:51
#, kde-format
msgid "Lorenzo Bonomi"
msgstr "Lorenzo Bonomi"

#: main.cpp:52
#, kde-format
msgid "testing"
msgstr "Testing"

#: main.cpp:54
#, kde-format
msgid "Brian Croom"
msgstr "Brian Croom"

#: main.cpp:55
#, kde-format
msgid "port to KGameRenderer"
msgstr "Porting til KGameRenderer"

#: main.cpp:57
#, kde-format
msgid "Viranch Mehta"
msgstr "Viranch Mehta"

#: main.cpp:58
#, kde-format
msgid "port to QtQuick"
msgstr "Porting til QtQuick"

#: mainwindow.cpp:108
#, kde-format
msgctxt "Key (shortcut) to toggle full screen"
msgid "F"
msgstr "F"

#: mainwindow.cpp:111
#, kde-format
msgid "Fire the ball"
msgstr "Skyt ballen"

#: mainwindow.cpp:120
#, kde-format
msgid "Skip level"
msgstr "Hopp over brett"

#: mainwindow.cpp:127
#, kde-format
msgid "Add life"
msgstr "Legg til liv"

#: mainwindow.cpp:137
#, kde-format
msgctxt "Key (shortcut) to pause the game"
msgid "P"
msgstr "P"

#: mainwindow.cpp:157
#, kde-format
msgid "Theme"
msgstr "Tema"

#: mainwindow.cpp:161
#, kde-format
msgctxt "General settings"
msgid "General"
msgstr "Generelt"

#: mainwindow.cpp:170
#, kde-format
msgid "   Time (hh:mm)"
msgstr "   Tid (tt:mm)"

#: mainwindow.cpp:183
#, kde-format
msgid "Starting a new game will end the current one!"
msgstr "Viss du startar eit nytt spel, vil du mista det du spelar på no."

#: mainwindow.cpp:184
#, kde-format
msgid "New Game"
msgstr "Nytt spel"

#: mainwindow.cpp:185
#, kde-format
msgid "Start a New Game"
msgstr "Start eit nytt spel"

#: mainwindow.cpp:223
#, kde-format
msgid "Game won!"
msgstr "Du vann!"

#: mainwindow.cpp:230
#, kde-format
msgid "Time (hh:mm)"
msgstr "Tid (tt:mm)"

#: mainwindow.cpp:313
#, kde-format
msgid ""
"Do you want to fire the ball on mouse click?\n"
"Answering Yes will make the game steal the\n"
"mouse cursor, pause the game to get\n"
"the cursor back."
msgstr ""
"Ønskjer du å skyta ballen med museknappen?\n"
"Viss du svarar ja, vil spelet stela musepeikaren.\n"
"Set spelet i pause for å få han tilbake."

#: mainwindow.cpp:317
#, kde-format
msgid "Fire on click?"
msgstr "Skyt ved klikk?"

#: mainwindow.cpp:318
#, kde-format
msgctxt "@action;button"
msgid "Use Mouse Click"
msgstr "Bruk museklikk"

#: mainwindow.cpp:319
#, kde-format
msgctxt "@action;button"
msgid "Ignore Mouse Click"
msgstr "Ignorer museklikk"

#: qml/logic.js:199
msgid "Well done! You won the game"
msgstr "Bra jobba! Du vann spelet!"

#: qml/logic.js:206 qml/main.qml:145
msgid "Level %1"
msgstr "Brett %1"

#: qml/logic.js:347
msgid "Game Paused!"
msgstr "Pause"

#: qml/logic.js:497
msgid "Oops! You have lost the ball!"
msgstr "Uff då! Du mista ballen!"

#: qml/logic.js:552
msgid "Game Over!"
msgstr "Spelet er slutt!"

#: qml/main.qml:115
msgid "Press %1 to fire the ball"
msgstr "Trykk «%1» for å skyta ballen"
